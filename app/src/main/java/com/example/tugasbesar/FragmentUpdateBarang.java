package com.example.tugasbesar;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentUpdateBarang#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentUpdateBarang extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    DBHelper DB;
    TextView namaTv, jenisTv;
    EditText jumlahText;
    Button updateBtn;

    public FragmentUpdateBarang() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentUpdateBarang.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentUpdateBarang newInstance(String param1, String param2) {
        FragmentUpdateBarang fragment = new FragmentUpdateBarang();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_update_barang, container, false);
        // Inflate the layout for this fragment
        namaTv = view.findViewById(R.id.namaBarangTv);
        jenisTv = view.findViewById(R.id.jenisBarangTv);
        jumlahText = view.findViewById(R.id.jumlah_barang);
        updateBtn = view.findViewById(R.id.updateBtn);
        DB = new DBHelper(getActivity());

        String namaIntent = getActivity().getIntent().getExtras().getString("nama");
        String jenisIntent = getActivity().getIntent().getExtras().getString("jenis");
        String jumlahIntent = getActivity().getIntent().getExtras().getString("jumlah");
        namaTv.setText(namaIntent);
        jenisTv.setText(jenisIntent);
        jumlahText.setText(jumlahIntent);

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String namaUpdate = namaTv.getText().toString();
                String value = jumlahText.getText().toString();
                int jumlahBaru = Integer.parseInt(value);

                if(jumlahText.equals("")){
                    Toast.makeText(getActivity(), "Jumlah Kosong", Toast.LENGTH_SHORT).show();
                }
                else{
                    Boolean upbarang = DB.updatejumlah(namaUpdate,jumlahBaru);
                    if(upbarang){
                        Toast.makeText(getActivity(), "Barang berhasil di-Update", Toast.LENGTH_SHORT).show();
                        jumlahText.onEditorAction(EditorInfo.IME_ACTION_DONE);
                    }
                    else{
                        Toast.makeText(getActivity(), "Barang gagal di-Update", Toast.LENGTH_SHORT).show();
                    }
                }

            }

        });

        return view;
    }
}