package com.example.tugasbesar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {
    public static final String DBNAME = "Tubes.db";

    public DBHelper(Context context) {
        super(context, "Tubes.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase MyDB) {
        MyDB.execSQL("CREATE TABLE pengguna(username varchar(100) PRIMARY KEY, password varchar(100))");
        MyDB.execSQL("CREATE TABLE barang(noid INTEGER PRIMARY KEY autoincrement, nama varchar(150), jenis varchar(100), jumlah INTEGER)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase MyDB, int oldVersion, int newVersion) {

    }

    public Boolean register(String username,String password){
        SQLiteDatabase MyDB = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("username", username);
        contentValues.put("password", password);
        long hasil = MyDB.insert("pengguna",null, contentValues);
        if(hasil==-1){
            return false;
        }
        else {
            return true;
        }
    }

    public Boolean updatepass(String username,String password){
        SQLiteDatabase MyDB = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("password", password);
        long hasil = MyDB.update("pengguna",contentValues,"username=?",new String[]{username});
        if(hasil==-1){
            return false;
        }
        else {
            return true;
        }
    }

    public Boolean updatejumlah(String nama,int jumlah){
        SQLiteDatabase MyDB = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("jumlah", jumlah);
        long hasil = MyDB.update("barang",contentValues,"nama=?",new String[]{nama});
        if(hasil==-1){
            return false;
        }
        else {
            return true;
        }
    }

    public Boolean baru(String nama,String jenis, int jumlah){
        SQLiteDatabase MyDB = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("nama", nama);
        contentValues.put("jenis", jenis);
        contentValues.put("jumlah", jumlah);
        long hasil = MyDB.insert("barang",null, contentValues);
        if(hasil==-1){
            return false;
        }
        else {
            return true;
        }
    }

    public Boolean delete(String nama){
        SQLiteDatabase MyDB = this.getWritableDatabase();
        long hasil = MyDB.delete("barang","nama=?", new String[]{nama});
        if(hasil==-1){
            return false;
        }
        else {
            return true;
        }
    }

    public Boolean checkbarang(String username){
        SQLiteDatabase MyDB = this.getWritableDatabase();
        Cursor cursor = MyDB.rawQuery("SELECT * FROM barang WHERE nama=?", new String[]{username});
        if(cursor.getCount()>0){
            return true;
        }
        else {
            return false;
        }
    }

    public Boolean checkuser(String username){
        SQLiteDatabase MyDB = this.getWritableDatabase();
        Cursor cursor = MyDB.rawQuery("SELECT * FROM pengguna WHERE username=?", new String[]{username});
        if(cursor.getCount()>0){
            return true;
        }
        else {
            return false;
        }
    }

    public Boolean checkuserpass(String username,String password){
        SQLiteDatabase MyDB = this.getWritableDatabase();
        Cursor cursor = MyDB.rawQuery("SELECT * FROM pengguna WHERE username=? and password=?", new String[]{username,password});
        if(cursor.getCount()>0){
            return true;
        }
        else {
            return false;
        }
    }

    public Cursor readalldata(){
        String query= "SELECT * FROM barang";
        SQLiteDatabase MyDB = this.getWritableDatabase();
        Cursor cursor= MyDB.rawQuery(query, null);
        return cursor;
    }


}
