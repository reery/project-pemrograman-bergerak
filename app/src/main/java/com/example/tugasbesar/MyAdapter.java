package com.example.tugasbesar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
//    private ArrayList<String> dataNama,dataJenis,dataJumlah;

    public ArrayList<Barang> baranglist;
    private OnNoteListener mOneNoteListener;
    DBHelper DB;
//    public MyAdapter(ArrayList<String> dataNama,ArrayList<String> dataJenis,ArrayList<String> dataJumlah){
//        this.dataNama=dataNama;
//        this.dataJenis=dataJenis;
//        this.dataJumlah=dataJumlah;
//    }
    public MyAdapter(ArrayList<Barang> baranglist, OnNoteListener onNoteListener){
        this.baranglist=baranglist;
        this.mOneNoteListener=onNoteListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row, parent, false);
        return new ViewHolder(view, mOneNoteListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Barang currentbarang = baranglist.get(position);
        holder.namabarang_tv.setText(currentbarang.getNama());
        holder.jenisbarang_tv.setText(currentbarang.getJenis());
        holder.jumlahbarang_tv.setText(currentbarang.getJumlah());
        DB = new DBHelper(holder.namabarang_tv.getContext());


        holder.delBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String namaHapus = currentbarang.getNama();

                AlertDialog.Builder builder = new AlertDialog.Builder(holder.namabarang_tv.getContext());
                builder.setCancelable(true);
                builder.setTitle("Hapus");
                builder.setMessage("Yakin ingin menghapus barang "+namaHapus);
                builder.setPositiveButton("Confirm",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Hapus
                                Boolean cekdel = DB.delete(namaHapus);
                                if (cekdel){
                                    Toast.makeText(v.getContext(),"Barang "+namaHapus+" dihapus", Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    Toast.makeText(v.getContext(),"Barang "+namaHapus+" gagal dihapus", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                builder.setNegativeButton(android.R.string.cancel,
                        new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });

    }

    public interface OnNoteListener{
        void OnNoteClick(int position);
    }

    @Override
    public int getItemCount() {
        return baranglist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView namabarang_tv,jenisbarang_tv,jumlahbarang_tv;
        Button delBtn;
        OnNoteListener onNoteListener;
        public ViewHolder(@NonNull View itemView, OnNoteListener onNoteListener) {
            super(itemView);
            namabarang_tv = itemView.findViewById(R.id.namatxt);
            jenisbarang_tv = itemView.findViewById(R.id.jenistxt);
            jumlahbarang_tv = itemView.findViewById(R.id.jumlahtxt);
            delBtn = itemView.findViewById(R.id.delBtn);
            this.onNoteListener = onNoteListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onNoteListener.OnNoteClick(getAdapterPosition());
        }


    }

    public void filterList(ArrayList<Barang> filteredList) {
        baranglist = filteredList;
        notifyDataSetChanged();
    }
}
