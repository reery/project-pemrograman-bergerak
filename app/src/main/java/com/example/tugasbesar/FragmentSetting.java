package com.example.tugasbesar;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentSetting#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentSetting extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    DBHelper DB;
    EditText passlama,passbaru1,passbaru2;
    Button settingBtn;

    public FragmentSetting() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentSetting.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentSetting newInstance(String param1, String param2) {
        FragmentSetting fragment = new FragmentSetting();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        // Inflate the layout for this fragment
        passlama = view.findViewById(R.id.password_lama);
        passbaru1 = view.findViewById(R.id.password_baru1);
        passbaru2 = view.findViewById(R.id.password_baru2);
        settingBtn = view.findViewById(R.id.settingBtn);
        DB = new DBHelper(getActivity());
        String username = getActivity().getIntent().getExtras().getString("username");

        settingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String lama = passlama.getText().toString();
                String baru1 = passbaru1.getText().toString();
                String baru2 = passbaru2.getText().toString();

                if(lama.equals("")||baru1.equals("")||baru2.equals("")){
                    Toast.makeText(getActivity(), "Password lama atau baru Kosong", Toast.LENGTH_SHORT).show();
                }
                else{
                    if (baru1.equals(baru2)){
                        Boolean cekuserpass = DB.checkuserpass(username,lama);
                        if (cekuserpass){
                            Boolean update =DB.updatepass(username,baru1);
                            if (update){
                                Toast.makeText(getActivity(), "Password berhasil diganti", Toast.LENGTH_SHORT).show();
                                passlama.setText("");
                                passbaru1.setText("");
                                passbaru2.setText("");
                                passlama.onEditorAction(EditorInfo.IME_ACTION_DONE);
                                passbaru1.onEditorAction(EditorInfo.IME_ACTION_DONE);
                                passbaru2.onEditorAction(EditorInfo.IME_ACTION_DONE);
                            }
                            else {
                                Toast.makeText(getActivity(), "Password gagal diganti", Toast.LENGTH_SHORT).show();
                            }

                        }
                        else {
                            Toast.makeText(getActivity(), "Password lama salah", Toast.LENGTH_SHORT).show();
                        }

                    }
                    else {
                        Toast.makeText(getActivity(), "Konfirmasi password baru salah", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
        return view;
    }
}