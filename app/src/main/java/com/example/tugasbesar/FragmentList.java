package com.example.tugasbesar;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentList#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentList extends Fragment implements MyAdapter.OnNoteListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    MyAdapter myAdapter;
    DBHelper DB;
    EditText cari;
    RecyclerView recyclerView;
    FloatingActionButton fab;
    ArrayList<String> namaBarang,jenisBarang,jumlahBarang;
    ArrayList<Barang> barang;

    public FragmentList() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentList.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentList newInstance(String param1, String param2) {
        FragmentList fragment = new FragmentList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        // Inflate the layout for this fragment
        DB = new DBHelper(getActivity());
        recyclerView = view.findViewById(R.id.listBarang);
        cari = view.findViewById(R.id.cariBarang);
        isiData();

        //refresh Data baru
        fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isiData();
                myAdapter.notifyDataSetChanged();
            }
        });

        //Cari barang
        cari.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

        return view;
    }

    private void filter(String keyword){
        ArrayList<Barang> filterBarang = new ArrayList<>();

        for (Barang nama : barang){
            if(nama.getNama().toLowerCase().contains(keyword.toLowerCase())){
                filterBarang.add(nama);
            }
            myAdapter.filterList(filterBarang);
        }
    }


    private void isiData(){

//        namaBarang = new ArrayList<String>();
//        jenisBarang = new ArrayList<String>();
//        jumlahBarang = new ArrayList<String>();
        barang = new ArrayList<>();

        Cursor data = DB.readalldata();
        if(data.moveToFirst()){
            do {
                barang.add(new Barang(data.getString(1), data.getString(2), data.getString(3)));
//                namaBarang.add(data.getString(1));
//                jenisBarang.add(data.getString(2));
//                jumlahBarang.add(data.getString(3));
            }while (data.moveToNext());
        }
        myAdapter = new MyAdapter(barang,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(myAdapter);
        myAdapter.notifyDataSetChanged();


    }

    @Override
    public void OnNoteClick(int position) {
        Intent intent = new Intent(getActivity(), MainActivity3.class);
        intent.putExtra("nama", barang.get(position).getNama());
        intent.putExtra("jenis", barang.get(position).getJenis());
        intent.putExtra("jumlah", barang.get(position).getJumlah());
        startActivity(intent);
    }
}