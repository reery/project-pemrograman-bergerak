package com.example.tugasbesar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager manager=getSupportFragmentManager();
        FragmentTransaction t=manager.beginTransaction();
        loginFragment f1 = new loginFragment();
        t.add(R.id.frame1, f1);
        t.commit();
    }

    public void regis() {
        FragmentManager manager1=getSupportFragmentManager();
        FragmentTransaction t2=manager1.beginTransaction();
        registerFragment f2 = new registerFragment();
        t2.replace(R.id.frame1, f2);
        t2.addToBackStack(null);
        t2.commit();
    }

    public void login() {
        FragmentManager manager1=getSupportFragmentManager();
        FragmentTransaction t2=manager1.beginTransaction();
        loginFragment f1 = new loginFragment();
        t2.replace(R.id.frame1, f1);
        t2.commit();
    }
}