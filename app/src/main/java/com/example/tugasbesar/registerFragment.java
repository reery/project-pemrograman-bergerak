package com.example.tugasbesar;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link registerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class registerFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    Button logBtn,registerBtn;
    EditText daftar_username,password1,password2;
    DBHelper DB;

    public registerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment registerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static registerFragment newInstance(String param1, String param2) {
        registerFragment fragment = new registerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        logBtn = view.findViewById(R.id.logBtn);
        registerBtn = (Button) view.findViewById(R.id.registerBtn);
        daftar_username = (EditText) view.findViewById(R.id.daftar_username);
        password1 = (EditText) view.findViewById(R.id.daftar_pass1);
        password2 = (EditText) view.findViewById(R.id.daftar_pass2);
        DB = new DBHelper(getActivity());

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = daftar_username.getText().toString();
                String pass1 = password1.getText().toString();
                String pass2 = password2.getText().toString();

                if(user.equals("")||pass1.equals("")||pass2.equals("")){
                    Toast.makeText(getActivity(), "Username atau Password Kosong", Toast.LENGTH_SHORT).show();
                }
                else{
                    if(pass1.equals(pass2)){
                        Boolean checkuser = DB.checkuser(user);
                        if (checkuser==false){
                            Boolean insert = DB.register(user,pass1);
                            if (insert==true){
                                Toast.makeText(getActivity(),"Pendaftaran Berhasil", Toast.LENGTH_SHORT).show();
                                daftar_username.setText("");
                                password1.setText("");
                                password2.setText("");
                                daftar_username.onEditorAction(EditorInfo.IME_ACTION_DONE);
                                password1.onEditorAction(EditorInfo.IME_ACTION_DONE);
                                password2.onEditorAction(EditorInfo.IME_ACTION_DONE);
                            }
                            else{
                                Toast.makeText(getActivity(),"Pendaftaran Gagal", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else{
                            Toast.makeText(getActivity(),"Username sudah ada", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        Toast.makeText(getActivity(), "Konfirmasi Password Salah", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        logBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity mn1 = (MainActivity) getActivity();
                mn1.login();
            }
        });
        return view;
    }
}