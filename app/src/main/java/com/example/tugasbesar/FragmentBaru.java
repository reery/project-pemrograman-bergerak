package com.example.tugasbesar;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentBaru#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentBaru extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String jenis[]={"Box","Pack","Bags"};
    Button baru;
    EditText nama,jumlah;
    Spinner spinner;
    DBHelper DB;

    public FragmentBaru() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentBaru.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentBaru newInstance(String param1, String param2) {
        FragmentBaru fragment = new FragmentBaru();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_baru, container, false);
        // Inflate the layout for this fragment
        baru = view.findViewById(R.id.baruBtn);
        spinner = view.findViewById(R.id.jenis);
        jumlah = view.findViewById(R.id.jumlah);
        nama = view.findViewById(R.id.nama_barang);
        DB = new DBHelper(getActivity());

        baru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String namabarang = nama.getText().toString();
                String jenisbarang = spinner.getSelectedItem().toString();
                String value = jumlah.getText().toString();
                int jumlahbarang = Integer.parseInt(value);

                if(namabarang.equals("")||value.equals("")){
                    Toast.makeText(getActivity(), "Nama atau Jumlah Kosong", Toast.LENGTH_LONG).show();
                }
                else{
                    Boolean checkbarang = DB.checkbarang(namabarang);
                    if (checkbarang==false){
                        Boolean insert = DB.baru(namabarang,jenisbarang,jumlahbarang);
                        if (insert==true){
                            Toast.makeText(getActivity(),"Barang Berhasil ditambahkan", Toast.LENGTH_SHORT).show();
                            nama.setText("");
                            jumlah.setText("");
                            nama.onEditorAction(EditorInfo.IME_ACTION_DONE);
                            jumlah.onEditorAction(EditorInfo.IME_ACTION_DONE);
                        }
                        else{
                            Toast.makeText(getActivity(),"Barang Gagal ditambahkan", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Toast.makeText(getActivity(),"Barang sudah ada", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });

        ArrayAdapter <String> AdapterList = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1 ,jenis);
        spinner.setAdapter(AdapterList);
        return view;
    }
}