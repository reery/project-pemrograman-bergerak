package com.example.tugasbesar;

public class Barang {

    private String jumlah;
    private String nama;
    private String jenis;

    public Barang(String nama, String jenis, String jumlah) {
        this.jenis = jenis;
        this.nama = nama;
        this.jumlah = jumlah;
    }
    public String getJumlah() {
        return jumlah;
    }
    public String getNama() {
        return nama;
    }
    public String getJenis() {
        return jenis;
    }
}
